package com.cashonline.exam.loans.repository;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.loans.model.Loan;
import com.cashonline.exam.utils.DBLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class LoanHibernateDAOTest {

    private int persistedLoanId;
    private int loanToPersistId;
    private LoanHibernateDAO dao;
    private Loan loanToPersist;

    @Before
    public void setUp() throws Exception {
        persistedLoanId = 1;
        loanToPersistId = 30;
        DBLoader.execute();
        dao = new LoanHibernateDAO();
        loanToPersist = new Loan(loanToPersistId, 30000, 1);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void loanDAOGetLoanTest() throws DBException {
        dao.openCurrentSessionwithTransaction();
        Loan recoveredLoan = dao.getLoanById(persistedLoanId);
        dao.closeCurrentSessionwithTransaction();
        assertNotNull(recoveredLoan);
    }

    @Test
    public void loanDAOInsertLoanTest() throws DBException {
        dao.openCurrentSessionwithTransaction();
        dao.insertLoan(loanToPersist);
        Loan recoveredLoan = dao.getLoanById(loanToPersistId);
        dao.closeCurrentSessionwithTransaction();
        assertNotNull(recoveredLoan);
    }

    @Test
    public void loanDAOGetLoansTest() throws DBException {
        dao.openCurrentSessionwithTransaction();
        List<Loan> recoveredLoans = dao.getLoans(0, 5, 1);
        dao.closeCurrentSessionwithTransaction();
        assertNotNull(recoveredLoans);
        assertEquals(5, recoveredLoans.size());
    }

    @Test
    public void loanDAOGetLoansTotalTest() throws DBException {
        dao.openCurrentSessionwithTransaction();
        int total = dao.getLoansTotal(1);
        dao.closeCurrentSessionwithTransaction();
        assertEquals(7, total);
    }
}