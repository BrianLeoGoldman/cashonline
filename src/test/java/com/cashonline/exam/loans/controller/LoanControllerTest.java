package com.cashonline.exam.loans.controller;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.loans.model.Loan;
import com.cashonline.exam.loans.service.LoanService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class LoanControllerTest {

    @InjectMocks
    private LoanController controller;

    @Mock
    private LoanService service;

    private int fakeLoanId, amount, user_id;
    private Loan fakeLoan;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        fakeLoanId = 24;
        amount = 30000;
        user_id = 1;
        fakeLoan = new Loan(fakeLoanId, amount, user_id);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getLoanById() throws DBException {
        Mockito.when(service.getLoanById(fakeLoanId)).thenReturn(fakeLoan);
        ResponseEntity e = controller.getLoanById("24");
        assertEquals(HttpStatus.OK, e.getStatusCode());
        assertTrue(e.hasBody());
        assertEquals(Loan.class, e.getBody().getClass());
    }

    @Test
    public void insertLoan() throws DBException {
        controller.insertLoan("24", "30000", "1");
        verify(service, times(1)).insertLoan(fakeLoan);
    }

    @Test
    public void getLoans() throws DBException {
        String page = "0";
        String size = "1";
        int user_id = 1;
        controller.getLoans(page, size, user_id);
        verify(service, times(1)).getLoans(Integer.parseInt(page), Integer.parseInt(size), user_id);
    }
}