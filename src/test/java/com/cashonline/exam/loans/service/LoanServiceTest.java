package com.cashonline.exam.loans.service;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.loans.model.Loan;
import com.cashonline.exam.loans.repository.LoanHibernateDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class LoanServiceTest {

    @InjectMocks
    private LoanService service;

    @Mock
    private LoanHibernateDAO dao;

    private int fakeLoanId;
    private Loan fakeLoan, loan1, loan2, loan3;
    List<Loan> loanList;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        fakeLoanId = 10;
        fakeLoan = new Loan(fakeLoanId, 30000, 1);
        loan1 = new Loan(11, 15000, 1);
        loan2 = new Loan(12, 25000, 1);
        loan3 = new Loan(13, 35000, 1);
        loanList = new ArrayList<>();
        loanList.add(loan1);
        loanList.add(loan2);
        loanList.add(loan3);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getLoanById() throws DBException {
        Mockito.when(dao.getLoanById(fakeLoanId)).thenReturn(fakeLoan);
        Loan actualLoan = service.getLoanById(fakeLoanId);

        assertNotNull(actualLoan);
        assertEquals(fakeLoan, actualLoan);
    }

    @Test
    public void insertLoan() throws DBException {
        service.insertLoan(fakeLoan);
        verify(dao, times(1)).insertLoan(fakeLoan);
    }

    @Test
    public void getLoans() throws DBException {
        Mockito.when(dao.getLoans(0, 5, 1)).thenReturn(loanList);
        List<Loan> response = service.getLoans(0, 5, 1);
        verify(dao, times(1)).getLoans(0, 5, 1);
        assertEquals(3, response.size());
    }

    @Test
    public void getLoansTotal() throws DBException {
        service.getLoansTotal(1);
        verify(dao, times(1)).getLoansTotal(1);
    }
}