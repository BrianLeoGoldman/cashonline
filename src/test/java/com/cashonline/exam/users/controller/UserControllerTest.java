package com.cashonline.exam.users.controller;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.users.model.User;
import com.cashonline.exam.users.service.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class UserControllerTest {

    @InjectMocks
    private UserController controller;

    @Mock
    private UserService service;

    private int fakeUserId;
    private String fakeUserEmail, first_name, last_name;
    private User fakeUser;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        fakeUserId = 10;
        fakeUserEmail = "test@mail.com";
        first_name = "John";
        last_name = "Test";
        fakeUser = new User(fakeUserId, fakeUserEmail, first_name, last_name);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getUserById() throws Exception {
        Mockito.when(service.getUserById(fakeUserId)).thenReturn(fakeUser);
        ResponseEntity e = controller.getUserById("10");
        assertEquals(HttpStatus.OK, e.getStatusCode());
        assertTrue(e.hasBody());
        assertEquals(User.class, e.getBody().getClass());
    }

    @Test
    public void insertUser() throws DBException {
        controller.insertUser("10", fakeUserEmail, first_name, last_name);
        verify(service, times(1)).insertUser(fakeUser);
    }

    @Test
    public void deleteUser() throws DBException {
        controller.deleteUser("10");
        verify(service, times(1)).deleteUser(fakeUserId);
    }
}