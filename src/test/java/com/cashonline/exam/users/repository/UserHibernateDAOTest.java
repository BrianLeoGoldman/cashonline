package com.cashonline.exam.users.repository;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.users.model.User;
import com.cashonline.exam.utils.DBLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserHibernateDAOTest {

    private int persistedUserId;
    private int userToPersistId;
    private UserHibernateDAO dao;
    private User userToPersist;

    @Before
    public void setUp() throws Exception {
        persistedUserId = 1;
        userToPersistId = 10;
        DBLoader.execute();
        dao = new UserHibernateDAO();
        userToPersist = new User(userToPersistId, "test@mail.com", "John", "Test");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void userDAOGetUserTest() throws DBException {
        dao.openCurrentSessionwithTransaction();
        User recoveredUser = dao.getUserById(persistedUserId);
        dao.closeCurrentSessionwithTransaction();
        assertNotNull(recoveredUser);
    }

    @Test
    public void userDAOInsertUserTest() throws DBException {
        dao.openCurrentSessionwithTransaction();
        dao.insertUser(userToPersist);
        User recoveredUser = dao.getUserById(userToPersistId);
        dao.closeCurrentSessionwithTransaction();
        assertNotNull(recoveredUser);
    }

    @Test
    public void userDAODeleteUserTest() throws DBException {
        dao.openCurrentSessionwithTransaction();
        dao.insertUser(userToPersist);
        dao.deleteUser(userToPersistId);
        User recoveredUser = dao.getUserById(userToPersistId);
        dao.closeCurrentSessionwithTransaction();
        assertNull(recoveredUser);
    }
}