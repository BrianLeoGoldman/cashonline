package com.cashonline.exam.users.service;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.users.model.User;
import com.cashonline.exam.users.repository.UserHibernateDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class UserServiceTest {

    @InjectMocks
    private UserService service;

    @Mock
    private UserHibernateDAO dao;

    private int fakeUserId;
    private User fakeUser;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        fakeUserId = 10;
        fakeUser = new User(fakeUserId, "test@mail.com", "John", "Test");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getUserById() throws DBException {
        Mockito.when(dao.getUserById(fakeUserId)).thenReturn(fakeUser);
        User actualUser = service.getUserById(fakeUserId);

        assertNotNull(actualUser);
        assertEquals(fakeUser, actualUser);
    }

    @Test
    public void insertUser() throws DBException {
        service.insertUser(fakeUser);
        verify(dao, times(1)).insertUser(fakeUser);
    }

    @Test
    public void deleteUser() throws DBException {
        service.deleteUser(fakeUserId);
        verify(dao, times(1)).deleteUser(fakeUserId);
    }
}