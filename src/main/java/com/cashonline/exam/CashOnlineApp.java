package com.cashonline.exam;

import com.cashonline.exam.utils.DBLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings("ALL")
@SpringBootApplication
public class CashOnlineApp {

    public static void main(String[] args) {
        DBLoader.execute();  // Comment this if you don't want database to be recreated each time
        SpringApplication.run(CashOnlineApp.class, args);

    }
}
