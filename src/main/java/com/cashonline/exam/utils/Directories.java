package com.cashonline.exam.utils;

public enum Directories {
    USER_CONTROLLER_LOG("src/main/resources/logs/user_controller_log.txt"),
    LOAN_CONTROLLER_LOG("src/main/resources/logs/loan_controller_log.txt"),
    USER_DAO_LOG("src/main/resources/logs/user_dao_log.txt"),
    LOAN_DAO_LOG("src/main/resources/logs/loan_dao_log.txt");


    private String message;

    Directories(final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


}
