package com.cashonline.exam.utils;

import com.cashonline.exam.exceptions.ErrorEnum;
import com.cashonline.exam.exceptions.InputException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

public class Validator {

    public int validateInt(final String input){
        int validatedInt;
        try {
            validatedInt = Integer.parseInt(input);
        }
        catch (IllegalArgumentException | NullPointerException e) {
            throw new InputException("Input not valid " + e.getMessage());
        }
        return validatedInt;
    }

    public void validateString(final String input) {
        try {
            Validate.notBlank(input, ErrorEnum.BLANK.getMessage());
        }
        catch (IllegalArgumentException | NullPointerException e) {
            throw new InputException(e.getMessage());
        }
        if (!StringUtils.isAlpha(input)) {
            throw new InputException(ErrorEnum.NOT_LETTERS.getMessage());
        }
    }

    public void validateEmail(final String input){
        if(!input.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$")){
            throw new InputException(ErrorEnum.NOT_LETTERS.getMessage());
        }
    }
}
