package com.cashonline.exam.utils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

    public static final String RESET = "\u001B[0m";
    public static final String ERROR = "\u001B[31m";
    public static final String INFO = "\u001B[32m";
    public static final String DB_ERROR = "\u001B[33m";
    public static final String OK = "\u001B[34m";
    public static final String COMMENT = "\u001B[37m";

    private BufferedWriter bufferedWriter = null;
    private FileWriter fileWriter = null;
    private String directory;

    public Logger(String directory){
        this.directory = directory;
    }

    public void log(String message, String level) {
        this.writeFileWithBufferedWriter(directory, message);
        System.out.println(getCurrentTime() + " " + level + message + RESET);
    }

    private void writeFileWithBufferedWriter(String filename, String message) {
        try {
            fileWriter = new FileWriter(filename, true);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.newLine();
            bufferedWriter.append(this.getCurrentTime() + " " + message);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getCurrentTime() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }
}
