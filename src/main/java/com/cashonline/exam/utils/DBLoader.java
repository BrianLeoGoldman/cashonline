package com.cashonline.exam.utils;

import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.DriverManager;

public class DBLoader {

    public static void execute() {

        String script = "src/main/resources/tables.sql";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            new ScriptRunner(DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cash_online", "root", "root"))
                    .runScript(new BufferedReader(new FileReader(script)));
        } catch (Exception e) {
            System.err.println(e);
        }
    }

}
