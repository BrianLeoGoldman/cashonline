package com.cashonline.exam.users.model;

import com.cashonline.exam.loans.model.Loan;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Data
@Entity
@Table(name = "user")
public class User implements Serializable {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "email")
    private String email;

    @Column(name = "first_name")
    private String first_name;

    @Column(name = "last_name")
    private String last_name;

    @OneToMany(mappedBy = "user_id", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Loan> loans;

    public User(){}

    public User(int id, String email, String first_name, String last_name){
        this.id = id;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.loans = new ArrayList<>();
    }

}
