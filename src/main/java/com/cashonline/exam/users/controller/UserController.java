package com.cashonline.exam.users.controller;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.exceptions.ErrorEnum;
import com.cashonline.exam.exceptions.ErrorWrapper;
import com.cashonline.exam.users.model.User;
import com.cashonline.exam.users.service.UserService;
import com.cashonline.exam.utils.Directories;
import com.cashonline.exam.utils.Logger;
import com.cashonline.exam.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping(value="/users")
public class UserController {

    @Autowired
    private UserService service;

    Logger logger = new Logger(Directories.USER_CONTROLLER_LOG.getMessage());
    Validator validator = new Validator();

    @RequestMapping(method = RequestMethod.GET, value="/{id}", produces = "application/json")
    public ResponseEntity getUserById(@PathVariable("id") String id) {
        try {
            int validatedInt = validator.validateInt(id);
            User user = service.getUserById(validatedInt);
            if(user == null){
                throw new DBException(ErrorEnum.USER_NULL.getMessage());
            }
            else {
                logger.log("Request successful for user with id " + validatedInt, Logger.OK);
                return ResponseEntity.ok(user);
            }
        } catch (Exception e) {
            logger.log(e.getMessage(), Logger.ERROR);
            return new ResponseEntity<>(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity insertUser(@RequestParam("id") String id,
                                     @RequestParam("email") String email,
                                     @RequestParam("first_name") String first_name,
                                     @RequestParam("last_name") String last_name) {
        try {
            int validatedInt = validator.validateInt(id);
            validator.validateEmail(email);
            validator.validateString(first_name);
            validator.validateString(last_name);
            User user = new User(validatedInt, email, first_name, last_name);
            String response = service.insertUser(user);
            logger.log("Created user with id " + validatedInt, Logger.OK);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            logger.log(e.getMessage(), Logger.ERROR);
            return new ResponseEntity<>(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity deleteUser(@RequestParam("id_delete") String id){
        try {
            int validatedInt = validator.validateInt(id);
            String response = this.service.deleteUser(validatedInt);
            logger.log("Deleted user with id " + validatedInt, Logger.OK);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            logger.log(e.getMessage(), Logger.ERROR);
            return new ResponseEntity<>(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
