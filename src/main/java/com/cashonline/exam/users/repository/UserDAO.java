package com.cashonline.exam.users.repository;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.users.model.User;


public interface UserDAO {

    User getUserById(int id) throws DBException;

    void insertUser(User user) throws DBException;

    void deleteUser(int id) throws DBException;
}
