package com.cashonline.exam.users.repository;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.exceptions.ErrorEnum;
import com.cashonline.exam.users.model.User;
import com.cashonline.exam.utils.Directories;
import com.cashonline.exam.utils.HibernateDAO;
import com.cashonline.exam.utils.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class UserHibernateDAO extends HibernateDAO implements UserDAO {

    Logger logger = new Logger(Directories.USER_DAO_LOG.getMessage());

    @Override
    public User getUserById(int id) throws DBException {
        try{
            User user = getCurrentSession().get(User.class, id);
            getCurrentSession().flush() ;
            return user;
        }
        catch(Exception e){
            logger.log(e.getMessage(), Logger.DB_ERROR);
            throw new DBException(ErrorEnum.GET_USER_ERROR.getMessage(), e);
        }
    }

    @Override
    public void insertUser(User user) throws DBException {
        try{
            getCurrentSession().save(user);
            getCurrentSession().flush() ;
        }
        catch(Exception e){
            logger.log(e.getMessage(), Logger.DB_ERROR);
            throw new DBException(ErrorEnum.INSERT_USER_ERROR.getMessage(), e);
        }
    }

    @Override
    public void deleteUser(int id) throws DBException {
        try{
            getCurrentSession().delete(getUserById(id));
            getCurrentSession().flush();
        }
        catch(Exception e){
            logger.log(e.getMessage(), Logger.DB_ERROR);
            throw new DBException(ErrorEnum.DELETE_USER_ERROR.getMessage(), e);
        }
    }
}
