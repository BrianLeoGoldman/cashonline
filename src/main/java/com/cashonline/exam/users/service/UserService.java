package com.cashonline.exam.users.service;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.users.model.User;
import com.cashonline.exam.users.repository.UserHibernateDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserHibernateDAO dao;

    public User getUserById(int id) throws DBException {
        dao.openCurrentSessionwithTransaction();
        User user = dao.getUserById(id);
        dao.closeCurrentSessionwithTransaction();
        return user;
    }

    public String insertUser(User user) throws DBException {
        dao.openCurrentSessionwithTransaction();
        dao.insertUser(user);
        dao.closeCurrentSessionwithTransaction();
        return "User inserted";
    }

    public String deleteUser(int id) throws DBException {
        dao.openCurrentSessionwithTransaction();
        dao.deleteUser(id);
        dao.closeCurrentSessionwithTransaction();
        return "User deleted";
    }
}
