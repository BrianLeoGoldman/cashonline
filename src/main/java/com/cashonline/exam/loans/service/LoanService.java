package com.cashonline.exam.loans.service;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.loans.model.Loan;
import com.cashonline.exam.loans.repository.LoanHibernateDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoanService {

    @Autowired
    private LoanHibernateDAO dao;

    public Loan getLoanById(int id) throws DBException {
        dao.openCurrentSessionwithTransaction();
        Loan loan = this.dao.getLoanById(id);
        dao.closeCurrentSessionwithTransaction();
        return loan;
    }

    public String insertLoan(Loan loan) throws DBException {
        dao.openCurrentSessionwithTransaction();
        this.dao.insertLoan(loan);
        dao.closeCurrentSessionwithTransaction();
        return "Loan inserted";
    }

    public List<Loan> getLoans(int page, int size, Integer user_id) throws DBException {
        dao.openCurrentSessionwithTransaction();
        List<Loan> loans = this.dao.getLoans(page, size, user_id);
        dao.closeCurrentSessionwithTransaction();
        return loans;
    }

    public int getLoansTotal(Integer user_id) throws DBException {
        dao.openCurrentSessionwithTransaction();
        int total = this.dao.getLoansTotal(user_id);
        dao.closeCurrentSessionwithTransaction();
        return total;
    }
}
