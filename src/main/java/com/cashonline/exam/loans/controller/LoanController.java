package com.cashonline.exam.loans.controller;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.exceptions.ErrorEnum;
import com.cashonline.exam.exceptions.ErrorWrapper;
import com.cashonline.exam.loans.model.Loan;
import com.cashonline.exam.loans.model.LoanResponse;
import com.cashonline.exam.loans.model.Paging;
import com.cashonline.exam.loans.service.LoanService;
import com.cashonline.exam.utils.Directories;
import com.cashonline.exam.utils.Logger;
import com.cashonline.exam.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/loans")
public class LoanController {

    @Autowired
    private LoanService loanService;

    Logger logger = new Logger(Directories.LOAN_CONTROLLER_LOG.getMessage());
    Validator validator = new Validator();

    @RequestMapping(method = RequestMethod.GET, value="/{id}", produces = "application/json")
    public ResponseEntity getLoanById(@PathVariable("id") String id) {
        try {
            int validatedInt = validator.validateInt(id);
            Loan loan = loanService.getLoanById(validatedInt);
            if(loan == null){
                throw new DBException(ErrorEnum.LOAN_NULL.getMessage());
            }
            else {
                logger.log("Request successful for loan with id " + validatedInt, Logger.OK);
                return ResponseEntity.ok(loan);
            }
        } catch (Exception e) {
            logger.log(e.getMessage(), Logger.ERROR);
            return new ResponseEntity<>(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity insertLoan(@RequestParam("id") String id,
                                     @RequestParam("amount") String amount,
                                     @RequestParam("user_id") String user_id) {
        try {
            int validatedId = validator.validateInt(id);
            int validatedAmount = validator.validateInt(amount);
            int validatedUserId = validator.validateInt(user_id);
            Loan loan = new Loan(validatedId, validatedAmount, validatedUserId);
            String response = this.loanService.insertLoan(loan);
            logger.log("Created loan with id " + validatedId, Logger.OK);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            logger.log(e.getMessage(), Logger.ERROR);
            return new ResponseEntity<>(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getLoans(@RequestParam("page") String page,
                                   @RequestParam("size") String size,
                                   @RequestParam(value = "user_id", required=false) Integer user_id) {
        try {
            int validatedPage = validator.validateInt(page);
            int validatedSize = validator.validateInt(size);
            List<Loan> loans = this.loanService.getLoans(validatedPage, validatedSize, user_id);
            int total = this.loanService.getLoansTotal(user_id);
            LoanResponse response = new LoanResponse(loans, new Paging(validatedPage, validatedSize, total));
            logger.log("Retrieved loans successfully", Logger.OK);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            logger.log(e.getMessage(), Logger.ERROR);
            return new ResponseEntity<>(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
