package com.cashonline.exam.loans.model;

import java.util.List;

public class LoanResponse {

    private final List<Loan> items;
    private final Paging paging;

    public LoanResponse(List<Loan> items, Paging paging) {
        this.items = items;
        this.paging = paging;
    }

    public List<Loan> getItems() {
        return items;
    }

    public Paging getPaging() {
        return paging;
    }
}
