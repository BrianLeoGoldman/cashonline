package com.cashonline.exam.loans.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "loan")
public class Loan {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "amount")
    private int amount;

    @JoinColumn(name = "user_id")
    private int user_id;

    public Loan(){}

    public Loan(int id, int amount, int user_id){
        this.id = id;
        this.amount = amount;
        this.user_id = user_id;
    }

}
