package com.cashonline.exam.loans.model;

public class Paging {

    private final int page;
    private final int size;
    private final int total;

    public Paging(int page, int size, int total) {
        this.page = page;
        this.size = size;
        this.total = total;
    }

    public int getPage() {
        return page;
    }

    public int getSize() {
        return size;
    }

    public int getTotal() {
        return total;
    }
}
