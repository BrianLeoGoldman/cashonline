package com.cashonline.exam.loans.repository;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.loans.model.Loan;

import java.util.List;

public interface LoanDAO {

    Loan getLoanById(int id) throws DBException;

    void insertLoan(Loan loan) throws DBException;

    List<Loan> getLoans(int page, int size, Integer user_id) throws DBException;

    int getLoansTotal(Integer user_id) throws DBException;
}
