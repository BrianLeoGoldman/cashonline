package com.cashonline.exam.loans.repository;

import com.cashonline.exam.exceptions.DBException;
import com.cashonline.exam.exceptions.ErrorEnum;
import com.cashonline.exam.loans.model.Loan;
import com.cashonline.exam.utils.Directories;
import com.cashonline.exam.utils.HibernateDAO;
import com.cashonline.exam.utils.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LoanHibernateDAO extends HibernateDAO implements LoanDAO {

    Logger logger = new Logger(Directories.LOAN_DAO_LOG.getMessage());

    @Override
    public Loan getLoanById(int id) throws DBException {
        try{
            Loan loan = getCurrentSession().get(Loan.class, id);
            getCurrentSession().flush() ;
            return loan;
        }
        catch(Exception e){
            logger.log(e.getMessage(), Logger.DB_ERROR);
            throw new DBException(ErrorEnum.GET_LOAN_ERROR.getMessage(), e);
        }
    }

    @Override
    public void insertLoan(Loan loan) throws DBException{
        try{
            getCurrentSession().save(loan);
            getCurrentSession().flush() ;
        }
        catch(Exception e){
            logger.log(e.getMessage(), Logger.DB_ERROR);
            throw new DBException(ErrorEnum.INSERT_LOAN_ERROR.getMessage(), e);
        }
    }

    @Override
    public List<Loan> getLoans(int page, int size, Integer user_id) throws DBException{
        try{
            Query query;
            if(user_id == null){
                query = getCurrentSession().createQuery("from Loan");
            }
            else {
                int id = user_id;
                query = getCurrentSession().createQuery("from Loan where user_id = " + id);
            }
            query.setMaxResults(size);  // size of page; each page displays size records (3, 4, 5, 6, 7)
            query.setFirstResult(size * page);  // starting position of the record (first record is 0, that is, 0, 1, 2, 3)
            List<Loan> result = query.list();
            getCurrentSession().flush() ;
            return result;
        }
        catch(Exception e){
            logger.log(e.getMessage(), Logger.DB_ERROR);
            throw new DBException(ErrorEnum.GET_LOAN_ERROR.getMessage(), e);
        }
    }

    @Override
    public int getLoansTotal(Integer user_id) throws DBException {
        try{
            Query query;
            if(user_id == null){
                query = getCurrentSession().createQuery("Select count (l.id) from Loan l");
            }
            else {
                int id = user_id;
                query = getCurrentSession().createQuery("Select count (l.id) from Loan l where user_id = " + id);
            }
            Long countResults = (Long) query.uniqueResult();
            getCurrentSession().flush() ;
            return countResults.intValue();
        }
        catch(Exception e){
            logger.log(e.getMessage(), Logger.DB_ERROR);
            throw new DBException(ErrorEnum.GET_LOAN_ERROR.getMessage(), e);
        }
    }

}
