package com.cashonline.exam.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class DBException extends Exception {


    public DBException(final String s) {
        super(s);
    }

    public DBException(final String s, final Throwable throwable) {
        super(s, throwable);
    }
}
