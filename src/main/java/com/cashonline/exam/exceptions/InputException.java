package com.cashonline.exam.exceptions;

public class InputException extends IllegalArgumentException {

    public InputException(final String s) {
        super(s);
    }

    public InputException(final String s, final Throwable throwable) {
        super(s, throwable);
    }

}
