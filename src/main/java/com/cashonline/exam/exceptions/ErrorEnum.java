package com.cashonline.exam.exceptions;

public enum ErrorEnum {
    USER_NULL("Failed to retrieve user"),
    GET_USER_ERROR("User cannot be found"),
    INSERT_USER_ERROR("User cannot be inserted"),
    DELETE_USER_ERROR("User cannot be deleted"),
    LOAN_NULL("Failed to retrieve loan"),
    GET_LOAN_ERROR("Loans cannot be founded"),
    INSERT_LOAN_ERROR("Loan cannot be inserted"),
    DELETE_LOAN_ERROR("Loan cannot be deleted"),
    DATABASE_ERROR("General data base error."),
    BLANK("Field should not be blank."),
    NULL("Field should no be null."),
    NOT_LETTERS("Field has invalid symbols.");


    private String message;

    ErrorEnum(final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


}
