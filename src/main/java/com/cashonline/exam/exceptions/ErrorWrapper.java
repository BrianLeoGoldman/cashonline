package com.cashonline.exam.exceptions;

/**
 * Wrapper class for errors (wrap a string into an object)
 */
public class ErrorWrapper {

    private String message;

    /**
     *
     * @param message {@link String}
     */
    public ErrorWrapper(final String message) {
        this.message = message;
    }

    /**
     * Gets message.
     *
     * @return message {@link String} Value of message.
     */
    public String getMessage() {
        return message;
    }
}
