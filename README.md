# CashOnline exam

## Requirements
1) DBMS for SQL (ex: MySQL, MariaDB, etc) installed
2) User **root** and password **root** are necessary
3) Create database **cash_online**

## Schemas
1) Tables can be created manually with tables.sql script
or by running DBLoader.execute() method on main. Script
will re create tables **user** and **loan** and inserts
basic data 

## Run application
1) Run main method in CashOnlineApp class
2) Run requests from browser or from an API development
environment like Postman

### Requests examples for users
* GET localhost:8080/users/1
* GET localhost:8080/users/10
* POST localhost:8080/users?id=7&email=pedro@mail.com&first_name=Pedro&last_name=W
* POST localhost:8080/users?id=1&email=juan@gmail.com&first_name=Juan&last_name=A
* DELETE localhost:8080/users?id_delete=1
* DELETE localhost:8080/users?id_delete=12

### Requests examples for loans
* GET localhost:8080/loans?page=0&size=3
* GET localhost:8080/loans?page=1&size=2&user_id=1
* POST localhost:8080/loans?id=12&amount=125000&user_id=3
* POST localhost:8080/loans?id=2&amount=40000&user_id=1
